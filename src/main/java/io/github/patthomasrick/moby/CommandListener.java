package io.github.patthomasrick.moby;

import io.github.patthomasrick.moby.commands.*;
import io.github.patthomasrick.moby.game.Game;
import io.github.patthomasrick.moby.game.TicTacToeGame;
import sx.blah.discord.Discord4J;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CommandListener {

    // some precanned responses
    public static final String REPLY_NOT_ADMIN = "Use must be an admin to use this command.";

    // static map of registered commands
    public static Map<String, Command> commandMap = new TreeMap<>();

    /*
    GAME STUFF
     */
    public static Map<String, Game> gameMap = new TreeMap<>();
    public static Map<Integer, Game> sessionMap = new TreeMap<>();
    private static Game mostRecentGame = null;
    private static Lock gameCoordLock = new ReentrantLock();

    // populate map with commands and games
    // ALL COMMANDS MUST BE ADDED HERE
    static {

        /*
        Register games
         */
        CommandListener.registerGame(new TicTacToeGame(null, null));


        /*
        Register commands
         */
        CommandListener.registerCommand(new ChoiceCommand());
//        CommandListener.registerCommand(new GameCommand());
        CommandListener.registerCommand(new GameEndCommand());
        CommandListener.registerCommand(new GameJoinCommand());
        CommandListener.registerCommand(new GameMoveCommand());
        CommandListener.registerCommand(new GameStartCommand());
        CommandListener.registerCommand(new HelloCommand());
        CommandListener.registerCommand(new HelpCommand());
        CommandListener.registerCommand(new RestartCommand());
        CommandListener.registerCommand(new SayCommand());
        CommandListener.registerCommand(new SourceCommand());
        CommandListener.registerCommand(new VoiceJoinCommand());
        CommandListener.registerCommand(new VoiceLeaveCommand());
        CommandListener.registerCommand(new YtPlayCommand());
        CommandListener.registerCommand(new YtSkipCommand());
        CommandListener.registerCommand(new YtStopCommand());

        CommandListener.registerCommand(new AliasCommand()); // HAS TO BE LAST
    }

    /**
     * Constructor for the command listener.
     */
    public CommandListener() {
        // start game GC thread
        Thread t = new Thread(new GameSessionGcThread());
        t.start();
    }

    /**
     * A helper method to more concisely add a command mapping.
     *
     * @param command Command object
     */
    public static void registerCommand(Command command) {
        for (String s : command.getAliases()) {
            CommandListener.commandMap.put(s, command);
        }
    }

    /**
     * Register a game as available for play.
     *
     * @param game (Some extension of) Game - IUser and IChannel are recommended to be null.
     */
    private static void registerGame(Game game) {
        CommandListener.gameMap.put(game.getName(), game);
    }

    /**
     * Add a newly created game session to the map and set it as the
     * most recently created game.
     * <p>
     * This might have some undefined behavior with threading and the
     * lack of locks here.
     *
     * @param game New instance of a Game.
     * @return id for the game in the map
     */
    public static int newGameSession(Game game) {
        // find an id for the session
        int i = 0;

        CommandListener.gameCoordLock.lock();
        while (CommandListener.sessionMap.get(i) != null) i++;

        // add the game and set it as the most recently created game
        CommandListener.sessionMap.put(i, game);
        CommandListener.mostRecentGame = game;
        CommandListener.gameCoordLock.unlock();

        return i;
    }

    /**
     * Getter for the most recently created game.
     *
     * @return most recent Game
     */
    public static Game getMostRecentGame() {
        return mostRecentGame;
    }

    /**
     * Watches for when messages are received. On the event that a message is received,
     * parse the message and process it as a command if it begins with "!".
     *
     * @param event event passed by Discord4J
     */
    @EventSubscriber
    public void onMessageReceived(MessageReceivedEvent event) {
        Thread t = new Thread(new CommandProcessor(event));
        t.start();
    }

    /**
     * A class implementing Runnable such that command parsing
     * is multi-threaded and does not block all other requests
     * while a possibly large command is being processed.
     */
    private static class CommandProcessor implements Runnable {

        MessageReceivedEvent event;

        /**
         * Constructor that takes a message received event to give
         * contextual support to the thread.
         *
         * @param event MessageReceivedEvent
         */
        CommandProcessor(MessageReceivedEvent event) {
            super();
            this.event = event;
        }

        @Override
        public void run() {
            // if the message starts with alias, behave differently
            String[] cmdArray = {event.getMessage().getContent()};
            if (!cmdArray[0].startsWith("!alias")) {
                // split into subcommands
                cmdArray = event.getMessage().getContent().split("&&");
            }

            for (String cmd : cmdArray) {
                cmd = cmd.trim();

                // split message into args
                String[] argArray = cmd.split(" ");

                // cannot have 0 args
                if (argArray.length == 0 || !argArray[0].startsWith(Moby.CMD_PREFIX)) {
                    return;
                }

                // extract command arg (remove prefix)
                String commandStr = argArray[0].substring(1);

                // put other args into arraylist
                List<String> argsList = new ArrayList<>(Arrays.asList(argArray));
                argsList.remove(0); // remove the cmd

                // run a command
                if (CommandListener.commandMap.containsKey(commandStr)) {
                    CommandListener.commandMap.get(commandStr).runCommand(event, argsList);
                } else {
                    Moby.sendMessage(
                            event.getChannel(),
                            String.format("`%s` is not a command.", cmd));
                }

                // sleep for 0.05 seconds so Moby doesn't catch on fire
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class GameSessionGcThread implements Runnable {

        public boolean alive = true;

        @Override
        public void run() {
            try {
                while (alive) {
                    for (int i : CommandListener.sessionMap.keySet()) {
                        Game session = CommandListener.sessionMap.get(i);

                        // remove dead and empty games
                        if (!session.isAlive() || session.getUsers().size() == 0) {
                            CommandListener.sessionMap.remove(i);
                            Discord4J.LOGGER.info(
                                    String.format(
                                            "Dereferenced %s's finished game of %s",
                                            session.getHost().getName(),
                                            session.getName()
                                    ));
                        }
                    }

                    // only look for dead games every 10 seconds
                    Thread.sleep(10000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
