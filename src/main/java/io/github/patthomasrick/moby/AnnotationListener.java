package io.github.patthomasrick.moby;

import sx.blah.discord.Discord4J;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.obj.ActivityType;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.StatusType;

public class AnnotationListener {

    @EventSubscriber
    public void onReadyEvent(ReadyEvent event) {
        /*
          Messages the developent channel upon logging in
         */
        for (IChannel c : event.getClient().getChannels()) {
            if (c.getLongID() == Moby.DEV_CHANNEL_ID) {
                Moby.sendMessage(c, "Logged in.");

                // list all commands
                StringBuilder cmdStr = new StringBuilder("Registered commands: ");
                for (String cmd : CommandListener.commandMap.keySet()) {
                    cmdStr.append(
                            String.format(
                                    "\t%s -> %s\n",
                                    cmd,
                                    CommandListener.commandMap.get(cmd).toString()
                            ));
                }
                Discord4J.LOGGER.info(cmdStr.toString());
            }
        }

        // initialize presence
        event.getClient().changePresence(StatusType.ONLINE, ActivityType.WATCHING, "BrainPop.com");
    }
}
