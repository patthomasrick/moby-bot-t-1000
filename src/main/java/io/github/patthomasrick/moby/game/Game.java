package io.github.patthomasrick.moby.game;

import io.github.patthomasrick.moby.Moby;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Abstract class for polymorphism
 */
public abstract class Game implements Runnable {

    private final int maxPlayers;
    protected Lock gameStartedLock = new ReentrantLock();
    protected Condition gameStartedCond = gameStartedLock.newCondition();
    private String name = "unnamed-game";
    private List<IUser> users = new ArrayList<IUser>();
    private boolean alive = true;
    private IChannel channel = null;
    private IUser host = null;

    /**
     * Constructor given a number of max players.
     *
     * @param maxPlayers the maximum number of players allowed
     */
    public Game(IUser host, IChannel channel, int maxPlayers) {
        this.channel = channel;
        this.host = host;
        this.maxPlayers = maxPlayers;
    }

    /**
     * Add a user to the user list.
     *
     * @param user IUser
     * @throws GameFullException game is full
     */
    public void userJoin(IUser user) throws GameFullException, GameFinishedException {

        if (this.users.size() >= this.maxPlayers) {
            this.sendMessage(String.format("%s, game is already full.", user.mention()));
            throw new GameFullException(String.format(
                    "User \"%s\" cannot join game \"%s\": Game is already full.",
                    user.getName(),
                    this.getName()
            ));
        } else if (!this.isAlive()) {
            this.sendMessage(String.format("%s, game has already finished.", user.mention()));
            throw new GameFinishedException(String.format(
                    "User \"%s\" cannot join game \"%s\": Game has already finished.",
                    user.getName(),
                    this.getName()
            ));
        } else {
            this.users.add(user);
            this.sendMessage(String.format("%s has joined the game.", user.mention()));
        }

        if (this.getUsers().size() == this.maxPlayers) {
            this.gameStartedLock.lock();
            this.gameStartedCond.signalAll();
            this.gameStartedLock.unlock();
        }
    }

    /**
     * For intelligent creation of new games from GC.
     *
     * @return a game of most precise subclass
     */
    public abstract Game newInstance(MessageReceivedEvent e);

    /**
     * Removes a user from the user list if the user is in the list.
     * Returns 0 for success, otherwise 1 for fail.
     *
     * @param user IUser to remove
     * @return 0 for success, 1 for fail.
     */
    public int userLeave(IUser user) {
        if (this.users.contains(user)) {
            this.users.remove(user);
            return 0;
        } else {
            return 1;
        }
    }

    public abstract void issueMove(MessageReceivedEvent e, List<String> args);

    /**
     * toString method.
     *
     * @return String name with current users
     */
    public String toString() {
        return String.format(
                "%s's %s - %d/%d",
                this.host.getName(),
                this.getName(),
                this.getUsers().size(),
                this.getMaxPlayers()
        );
    }

    public void sendMessage(String s) {
        if (this.channel != null) {
            Moby.sendMessage(
                    this.channel,
                    String.format(
                            "[%s's %s - %d/%d] %s",
                            this.host.getName(),
                            this.getName(),
                            this.getUsers().size(),
                            this.getMaxPlayers(),
                            s
                    )
            );
        }
    }
    

    /**
     * Getter for game's name.
     *
     * @return String name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for game's name.
     *
     * @param value name to be set
     */
    public void setName(String value) {
        this.name = value.replace(" ", "-");
    }

    /**
     * Getter for the maximum number of players
     *
     * @return int
     */
    public int getMaxPlayers() {
        return maxPlayers;
    }

    /**
     * Getter for the list of users in the game.
     *
     * @return List of IUser
     */
    public List<IUser> getUsers() {
        return users;
    }

    /**
     * Is the current game alive (not finished)
     *
     * @return boolean
     */
    public boolean isAlive() {
        return alive;
    }

    /**
     * Set if the game is alive. Settings to false kills the game thread if it is running, or otherwise
     * prevents he game loop from starting.
     *
     * @param alive boolean
     */
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    /**
     * Get the user that created the Game.
     *
     * @return IUser
     */
    public IUser getHost() {
        return this.host;
    }

    /**
     * Get the channel that the game was started in.
     *
     * @return Discord channel
     */
    public IChannel getChannel() {
        return channel;
    }

    /**
     * Exception thrown when a user tries to join a full game.
     */
    public static class GameFullException extends Exception {
        public GameFullException(String msg) {
            super(msg);
        }
    }

    /**
     * Exception thrown when a user tries to join a finished game.
     */
    public static class GameFinishedException extends Exception {
        public GameFinishedException(String msg) {
            super(msg);
        }
    }
}
