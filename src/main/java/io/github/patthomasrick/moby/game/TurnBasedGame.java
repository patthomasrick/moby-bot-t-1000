package io.github.patthomasrick.moby.game;

import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

abstract class TurnBasedGame extends Game {

    Lock turnLock = new ReentrantLock();
    Condition moveCond = turnLock.newCondition();

    /**
     * Constructor given a number of max players.
     *
     * @param host       IUser that created the lobby
     * @param channel    IChannel that the lobby was created in
     * @param maxPlayers the maximum number of players allowed
     */
    TurnBasedGame(IUser host, IChannel channel, int maxPlayers) {
        super(host, channel, maxPlayers);
    }
}
