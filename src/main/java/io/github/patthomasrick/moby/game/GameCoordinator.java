package io.github.patthomasrick.moby.game;

import io.github.patthomasrick.moby.Moby;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Coordinator. Essentially keeps track of all games.
 */
@Deprecated
public class GameCoordinator {

    private Map<String, Game> gameMap = new TreeMap<>();
    private Map<Integer, Game> currentGames = new TreeMap<>();

    private int gameCounter = 0;

    private Game recentGame = null;

    public GameCoordinator() {
        //
        // ADD NEW GAMES HERE
        //
        this.registerGame(new TicTacToeGame(null, null));
    }

    /**
     * Method to register games that can be played on the
     * game coordinator.
     *
     * @param g some extension of the Game class (preferably)
     */
    private void registerGame(Game g) {
        this.gameMap.put(g.getName(), g);
    }

    /**
     * Interfaces the Command system with the game system. Essentially looks at the arguments
     * passed with the !game call and keeps the MessageReceivedEvent as context.
     *
     * @param e    MessageReceivedEvent
     * @param args Strings
     * @throws GcInvalidCommand thrown if there are no arguments
     */
    public void issueCommand(MessageReceivedEvent e, List<String> args) throws GcInvalidCommand {
        if (args.size() == 0) {
            throw new GcInvalidCommand("Game command has no arguments.");
        }

        switch (args.get(0)) {
            /*
             * Starts a game.
             */
            case "start":
                if (args.size() == 2) {

                    // user cannot be in multiple games
                    for (int key : this.currentGames.keySet()) {
                        Game g = this.currentGames.get(key);
                        if (g.getUsers().contains(e.getAuthor())) {
                            Moby.sendMessage(e.getChannel(),
                                    e.getAuthor().mention() + ", you cannot be in multiple games.");
                            return;
                        }
                    }

                    if (this.gameMap.containsKey(args.get(1))) {
                        Game g = this.gameMap.get(args.get(1)).newInstance(e);

                        try {
                            g.userJoin(e.getAuthor());
                        } catch (Game.GameFullException | Game.GameFinishedException err) {
                            // if this happens, g.maxPlayers has to be >= 0
                            err.printStackTrace();
                        }

                        this.currentGames.put(this.gameCounter, g);
                        this.gameCounter++;
                        this.recentGame = g;

                        // start the game thread
                        Thread t = new Thread(g);
                        t.start();

                        g.sendMessage(String.format(
                                "To join this game, type `!game join %d`",
                                this.gameCounter - 1
                        ));
                    }
                } else {
                    e.getMessage().reply(
                            String.format("List of available games: %s", this.gameMap.keySet().toString()));
                }
                break;

            /*
             * Performs a move in a game
             */
            case ("move"):
                // find the game that the issuer is a part of
                Game target = null;
                for (int key : this.currentGames.keySet()) {
                    Game g = this.currentGames.get(key);
                    if (g.getUsers().contains(e.getAuthor())) {
                        target = g;
                        break;
                    }
                }

                // move if the user is actually in a game
                if (target == null) {
                    e.getMessage().reply("You are not in a game.");
                } else {
                    target.issueMove(e, args);
                }
                break;

            /*
             * Joins a game
             */
            case ("join"):
                // find which game to exactly join
                Game joinedGame = null;
                if (args.size() == 2) {
                    int key = Integer.parseInt(args.get(1));
                    if (this.currentGames.containsKey(key)) {
                        joinedGame = this.currentGames.get(key);
                    }
                } else if (args.size() == 1) {
                    // join the most recent game if no number is provided
                    joinedGame = this.recentGame;
                }

                try {
                    assert joinedGame != null;
                    joinedGame.userJoin(e.getAuthor());
                } catch (Game.GameFullException | Game.GameFinishedException err) {
                    err.printStackTrace();
                }
                break;

            default:
                break;
        }
    }

    /**
     * Exception when an invalid game command is sent to the coordinator.
     */
    public static class GcInvalidCommand extends Exception {
        GcInvalidCommand(String msg) {
            super(msg);
        }
    }
}
