package io.github.patthomasrick.moby.game;

import sx.blah.discord.Discord4J;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ThreadLocalRandom;

public class TicTacToeGame extends TurnBasedGame {

    // make the board a 3x3 array
    private int board[][] = new int[3][3];

    // map the users to 1 and 2, and those correspond to which user moves first and is X vs O
    private Map<Integer, String> userMarkerMap = new TreeMap<>();

    // the last move before the main loop was resumed
    private int[] lastMove = new int[2];

    // keeps track of the current player's turn
    private int currentUser;

    /**
     * Constructor. Mirrors Game constructor, zeros
     * board array.
     *
     * @param host    IUser that started the lobby
     * @param channel IChannel that the lobby was started in
     */
    public TicTacToeGame(IUser host, IChannel channel) {
        super(host, channel, 2);

        this.setName("tictactoe");

        // initialize board
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.board[i][j] = 0;
            }
        }
    }

    @Override
    public Game newInstance(MessageReceivedEvent e) {
        return new TicTacToeGame(e.getAuthor(), e.getChannel());
    }

    @Override
    public void issueMove(MessageReceivedEvent e, List<String> args) {
        // check if its even the right player
        if (this.userMarkerMap.get(this.currentUser).equals(e.getAuthor().mention())) {
            // validate inputs
            int row = -1, col = -1;
            if (args.size() == 1 && args.get(0).length() == 2) {
                row = args.get(0).toLowerCase().toCharArray()[0] - 'a';
                col = Character.getNumericValue(args.get(0).toCharArray()[1]) - 1;
            } else if (args.size() == 2) {
                row = args.get(0).toLowerCase().toCharArray()[0] - 'a';
                col = Integer.parseInt(args.get(1)) - 1;
            }

            if (row < 0 || row >= 3 || col < 0 || col >= 3) {
                this.sendMessage(e.getAuthor().mention() +
                        ", invalid move. Use the form `!game move <A, B, C> <1, 2, 3>`");
                return;
            }

            this.turnLock.lock();

            this.lastMove[0] = row;
            this.lastMove[1] = col;

            this.moveCond.signal();
            this.turnLock.unlock();
        } else {
            this.sendMessage(String.format("%s, it is not your turn.", e.getAuthor().mention()));
        }
    }

    @Override
    public void run() {
        try {
            // wait for game to start
            this.gameStartedLock.lock();
            while (this.getUsers().size() != 2) {
                this.gameStartedCond.await();
            }

            // notify the channel that the game is starting
            Discord4J.LOGGER.info("Game started.");
            this.sendMessage("Game has started.");

            // choose starting player
            this.currentUser = ThreadLocalRandom.current().nextInt(1, 2 + 1);

            // map the users to x and o
            this.userMarkerMap.put(1, this.getUsers().get(this.currentUser % 2).mention());
            this.userMarkerMap.put(2, this.getUsers().get((this.currentUser + 1) % 2).mention());

            this.sendMessage(
                    String.format(
                            "%s was chosen to go first. To move, Use the form `!game move <A, B, C> <1, 2, 3>`",
                            this.userMarkerMap.get(this.currentUser)
                    ));
            this.sendMessage(
                    String.format(
                            "%s\nIt is now %s's turn.",
                            this.boardToString(),
                            this.userMarkerMap.get(this.currentUser)
                    )
            );

            // main loop
            while (this.isAlive()) {
                // wait for a move to be entered
                this.turnLock.lock();
                moveCond.await();

                // make a move if the board is clear there
                if (this.board[lastMove[0]][lastMove[1]] == 0) {
                    // make the move
                    this.board[lastMove[0]][lastMove[1]] = this.currentUser;

                    // calculate the next player's turn
                    this.currentUser = this.currentUser % 2 + 1;

                    int winCheck = this.checkWin();

                    if (winCheck == 0) {
                        // notify
                        this.sendMessage(
                                String.format(
                                        "%s\nIt is now %s's turn.",
                                        this.boardToString(),
                                        this.userMarkerMap.get(this.currentUser)
                                )
                        );
                    } else if (winCheck == 3) {
                        this.sendMessage(
                                String.format(
                                        "%s\nThe game was a draw!.",
                                        this.boardToString()
                                )
                        );
                        this.setAlive(false);
                    } else {
                        this.sendMessage(
                                String.format(
                                        "%s\n%s has won!.",
                                        this.boardToString(),
                                        this.userMarkerMap.get(winCheck)
                                )
                        );
                        this.setAlive(false);
                    }
                } else {
                    this.sendMessage(String.format(
                            "You cannot make a move here. It is still %s's turn.",
                            this.userMarkerMap.get(this.currentUser)));
                }

                this.turnLock.unlock();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                this.gameStartedLock.unlock();
                this.turnLock.unlock();
            } catch (IllegalMonitorStateException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Check to see if a player has won.
     *
     * @return 0 for nobody won, otherwise 1, 2, or 3 (corresponding to the
     * userMarkerMap or 3 for draw)
     */
    private int checkWin() {
        // check if draw
        boolean foundZero = false;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == 0) foundZero = true;
            }
        }
        if (!foundZero) return 3;

        // check if player won
        for (int i = 0; i < 3; i++) {
            if (this.board[i][0] != 0 && // check horizontally
                    this.board[i][0] == this.board[i][1] &&
                    this.board[i][1] == this.board[i][2]) {
                return this.board[i][0];
            } else if (this.board[0][i] != 0 && // check veritcally
                    this.board[0][i] == this.board[1][i] &&
                    this.board[1][i] == this.board[2][i]) {
                return this.board[0][i];
            }
        }

        // check diagonal 1
        if (this.board[0][0] != 0 &&
                this.board[0][0] == this.board[1][1] &&
                this.board[1][1] == this.board[2][2]) {
            return this.board[0][0];
        }

        // check diagonal 2
        if (this.board[0][2] != 0 &&
                this.board[0][2] == this.board[1][1] &&
                this.board[1][1] == this.board[2][0]) {
            return this.board[0][2];
        }

        return 0;
    }

    /**
     * Put the board in a readable format
     *
     * @return String of board
     */
    private String boardToString() {
        StringBuilder out = new StringBuilder();
        out.append("```\n   1 | 2 | 3\n");
        for (int i = 0; i < 3; i++) {

            out.append((char) ('A' + i));
            out.append(' ');

            for (int j = 0; j < 3; j++) {
                switch (board[i][j]) {
                    case 0:
                        out.append("   ");
                        break;
                    case 1:
                        out.append(" X ");
                        break;
                    case 2:
                        out.append(" O ");
                        break;
                }
                if (j != 2) out.append("|");
            }
            if (i != 2) out.append("\n  ---+---+---\n");
        }
        out.append("```");
        return out.toString();
    }
}
