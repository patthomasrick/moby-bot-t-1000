package io.github.patthomasrick.moby.commands;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;

@Deprecated
public class GameCommand extends Command {

    public GameCommand() {
        this.aliases = new String[]{"game"};
        this.helpStr = "Multiple uses. ";
        this.usageStr = "!game [args]";

        this.hidden = true; // internal command
    }

    @Override
    public void runCommand(MessageReceivedEvent event, List<String> args) {
//        try {
//            Moby.gameCoordinator.issueCommand(event, args);
//        } catch (GameCoordinator.GcInvalidCommand e) {
//            Discord4J.LOGGER.error("Game command not found: " + args.toString());
//            e.printStackTrace();
//        }
    }
}
