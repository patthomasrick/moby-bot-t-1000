package io.github.patthomasrick.moby.commands;

import io.github.patthomasrick.moby.CommandListener;
import io.github.patthomasrick.moby.Moby;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static java.lang.Thread.sleep;

public class HelpCommand extends Command {

    public HelpCommand() {
        aliases = new String[]{"?", "help"};
        helpStr = "Sends a list of commands and their usages.";
        usageStr = "!?";
    }

//    public static void main(String args[]) {
//        String out = String.format("%s\n```", "@pat");
//        List<Command> uniqueCommands = new ArrayList<>();
//
//        for (String k : CommandListener.commandMap.keySet()) {
//            // get command
//            Command c = CommandListener.commandMap.get(k);
//
//            // test if command has already been processed (to deal with aliases)
//            if (!uniqueCommands.contains(c)) {
//                // disable repeats from now on
//                uniqueCommands.add(c);
//
//                // concat name from alias
//                String name = c.getAliases()[0];
//                for (String a : c.getAliases()) {
//                    if (!a.equals(name)) {
//                        name += ", " + a;
//                    }
//                }
//
//                // add help to output
//                out = String.format("%s\n%s\n    Usage: %s\n    Desc: %s\n", out, name, c.getUsageStr(), c.getHelpStr());
//            }
//        }
//        out += "```";
//
//        System.out.println(out);
//    }

    @Override
    public void runCommand(MessageReceivedEvent event, List<String> args) {

        ArrayList<String> out = new ArrayList<>();
        StringBuilder outCurrent = new StringBuilder(String.format("%s\n```\n", event.getAuthor().mention()));
        List<Command> uniqueCommands = new ArrayList<>();

        for (String commandKey : CommandListener.commandMap.keySet()) {
            // get command
            Command command = CommandListener.commandMap.get(commandKey);

            // test if command has already been processed (to deal with aliases)
            if (!uniqueCommands.contains(command)) {
                // disable repeats from now on
                uniqueCommands.add(command);

                // only continue if the command is not hidden
                if (command.hidden) {
                    continue;
                }

                // concat name from alias
                StringBuilder name = new StringBuilder(command.getAliases()[0]);
                for (int i = 1; i < command.getAliases().length; i++) {
                    name.append(", ");
                    name.append(command.getAliases()[i]);
                }

                String outPart = String.format("%s\n    Usage: %s\n    Desc: %s\n",
                        name.toString(), command.getUsageStr(), command.getHelpStr());
                if (outCurrent.length() + outPart.length() + 3 > 2000) {
                    // end current help message
                    outCurrent.append("```");

                    // start new
                    out.add(outCurrent.toString());
                    outCurrent = new StringBuilder("```");
                }
                outCurrent.append(outPart);
            }
        }
        outCurrent.append("\n```");
        out.add(outCurrent.toString());

        for (String s : out) {
            Moby.sendMessage(event.getChannel(), s);

            // sleep the thread for 10 millis so messages get sent in the right order
            try {
                sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}