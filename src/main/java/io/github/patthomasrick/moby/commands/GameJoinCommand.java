package io.github.patthomasrick.moby.commands;

import io.github.patthomasrick.moby.CommandListener;
import io.github.patthomasrick.moby.Moby;
import io.github.patthomasrick.moby.game.Game;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;

public class GameJoinCommand extends Command {

    public GameJoinCommand() {
        this.aliases = new String[]{"gamejoin", "joingame", "gjoin", "joing"};
        this.helpStr = "Join a game. Typing only !gamejoin with no id will join the latest game.";
        this.usageStr = "!gamejoin [id]";

        this.hidden = false; // internal command
    }

    @Override
    public void runCommand(MessageReceivedEvent event, List<String> args) {

        Game session = null;

        if (args.size() == 0) {
            session = CommandListener.getMostRecentGame();
        } else {
            try {
                int key = Integer.parseInt(args.get(0));
                session = CommandListener.sessionMap.get(key);
            } catch (NumberFormatException e) {
                event.getMessage().reply("The id in `!gamejoin id` must be numerical (0-9).");
            }
        }

        // join the session
        try {
            if (session != null) {
                session.userJoin(event.getAuthor());
            } else {
                Moby.reply(event, "Could not join game: Session could not be found.");
            }
        } catch (Game.GameFullException e) {
            Moby.reply(event, "Could not join game: Session is already full.");
            e.printStackTrace();
        } catch (Game.GameFinishedException e) {
            Moby.reply(event, "Could not join game: Session has already finished.");
            e.printStackTrace();
        }
    }
}
