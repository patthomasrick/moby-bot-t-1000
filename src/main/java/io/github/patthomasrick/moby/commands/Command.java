package io.github.patthomasrick.moby.commands;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.Arrays;
import java.util.List;

public class Command implements CommandInterface, Comparable {

    protected String[] aliases;
    protected String helpStr;
    protected String usageStr;

    // if false, the command will not show up in the help printout
    boolean hidden = false;

    boolean isAliasedCommand = false;

    public Command() {
        aliases = new String[]{"command"};
        helpStr = "Command.";
        usageStr = "!command arg1 arg2 ... argn";
    }

    public void runCommand(MessageReceivedEvent event, List<String> args) {
    }

    @Override
    public String toString() {
        return this.getUsageStr();
    }

    @Override
    public String[] getAliases() {
        return this.aliases;
    }

    @Override
    public String getHelpStr() {
        return this.helpStr;
    }

    @Override
    public String getUsageStr() {
        return this.usageStr;
    }

    @Override
    public boolean isAliasedCommand() {
        return this.isAliasedCommand;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Command) {
            Command cmd = (Command) o;
            List cmdAliases = Arrays.asList(cmd.aliases);
            boolean hasCommon = false;
            for (String alias : this.aliases) {
                if (cmdAliases.contains(alias)) {
                    hasCommon = true;
                    break;
                }
            }

            if (hasCommon) {
                return 0;
            }
        }
        return 1;
    }
}
