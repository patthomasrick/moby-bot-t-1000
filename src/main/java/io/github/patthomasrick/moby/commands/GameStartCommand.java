package io.github.patthomasrick.moby.commands;

import io.github.patthomasrick.moby.CommandListener;
import io.github.patthomasrick.moby.Moby;
import io.github.patthomasrick.moby.game.Game;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;

public class GameStartCommand extends Command {

    public GameStartCommand() {
        this.aliases = new String[]{"gamestart", "startgame"};
        this.helpStr = "Start a game.";
        this.usageStr = "!gamestart [game name]";

        this.hidden = false; // internal command
    }

    @Override
    public void runCommand(MessageReceivedEvent event, List<String> args) {

        // if the user provides no arguments, list the avaliable games to start
        if (args.size() == 0) {
            StringBuilder out = new StringBuilder(event.getAuthor().mention());
            out.append(", list of available games: `");
            for (String s : CommandListener.gameMap.keySet()) {
                out.append(s);
                out.append("`, `");
            }
            out.delete(out.length() - 3, out.length());

            Moby.sendMessage(
                    event.getChannel(),
                    out.toString()
            );
        }


        // disallow users being in multiple games
        for (Game session : CommandListener.sessionMap.values()) {
            if (session.getUsers().contains(event.getAuthor())) {
                Moby.reply(event, "You cannot be in multiple games.");
                return;
            }
        }


        // make a new session of the desired game
        Game session = CommandListener.gameMap.get(args.get(0)).newInstance(event);
        if (session == null) return;


        // join the new session
        try {
            session.userJoin(event.getAuthor());
        } catch (Game.GameFullException | Game.GameFinishedException e) {
            // these errors shouldn't happen
            e.printStackTrace();
        }


        // make the session in the command listener
        int id = CommandListener.newGameSession(session);

        // start the session's thread
        Thread t = new Thread(session);
        t.start();

        // notify other users to join
        session.sendMessage(String.format(
                "To join this game, type `!game join %d`",
                id
        ));
    }
}
