package io.github.patthomasrick.moby.commands;

import io.github.patthomasrick.moby.audio.AudioPlayer;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;

public class YtStopCommand extends Command {

    public YtStopCommand() {
        aliases = new String[]{"ytstop", "stop", "shutup", "bequiet"};
        helpStr = "Stops the current audio stream.";
        usageStr = "!ytstop";
    }

    @Override
    public void runCommand(MessageReceivedEvent event, List<String> args) {
        AudioPlayer.stop(event.getChannel());
    }
}
