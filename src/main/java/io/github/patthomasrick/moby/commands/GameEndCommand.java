package io.github.patthomasrick.moby.commands;

import io.github.patthomasrick.moby.CommandListener;
import io.github.patthomasrick.moby.Moby;
import io.github.patthomasrick.moby.game.Game;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;

public class GameEndCommand extends Command {

    public GameEndCommand() {
        this.aliases = new String[]{"gameend", "endgame", "abandon", "ragequit"};
        this.helpStr = "End the current game immediately.";
        this.usageStr = "!gameend";

        this.hidden = false; // internal command
    }

    @Override
    public void runCommand(MessageReceivedEvent event, List<String> args) {

        int sessionId = -1;

        // find the user's session
        for (int id : CommandListener.sessionMap.keySet()) {
            Game session = CommandListener.sessionMap.get(id);
            if (session.getUsers().contains(event.getAuthor())) {
                sessionId = id;
                break;
            }
        }

        // end the game
        if (sessionId != -1) {
            Game session = CommandListener.sessionMap.get(sessionId);
            session.setAlive(false);

            // dereference in hopes that Java's GC will sweep it up eventually
            CommandListener.sessionMap.remove(sessionId);

            Moby.reply(event, "The game has been ended.");
        }
    }
}
