package io.github.patthomasrick.moby.commands;

import io.github.patthomasrick.moby.CommandListener;
import io.github.patthomasrick.moby.Moby;
import io.github.patthomasrick.moby.game.Game;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;

public class GameMoveCommand extends Command {

    public GameMoveCommand() {
        this.aliases = new String[]{"gamemove", "move", "gm"};
        this.helpStr = "Make a move in your current game.";
        this.usageStr = "!gamemove [args]";

        this.hidden = false; // internal command
    }

    @Override
    public void runCommand(MessageReceivedEvent event, List<String> args) {

        Game joinedSession = null;

        // find the user's session
        for (Game session : CommandListener.sessionMap.values()) {
            if (session.getUsers().contains(event.getAuthor())) {
                joinedSession = session;
                break;
            }
        }

        // issue the move if the user is in a session
        if (joinedSession != null) {
            joinedSession.issueMove(event, args);
        } else {
            Moby.reply(event, "You are not in a game.");
        }
    }
}
