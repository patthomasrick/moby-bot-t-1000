package io.github.patthomasrick.moby.commands;

import io.github.patthomasrick.moby.CommandListener;
import io.github.patthomasrick.moby.Moby;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.Permissions;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class AliasCommand extends Command {

    private final static String ALIASES_TXT = "aliases.txt";

    public AliasCommand() {
        aliases = new String[]{"alias"};
        helpStr = "Makes a shortcut to another command with arguments.";
        usageStr = "!alias command arg0 arg1 ... argn";

        this.loadAliases();
    }

    /**
     * Load saved aliases from a file.
     * <p>
     * Aliases are saved in the following format:
     * aliasname command arg0 arg1 ... argn
     */
    private void loadAliases() {
        // initialize scanner
        Scanner reader;
        try {
            reader = new Scanner(new File(AliasCommand.ALIASES_TXT));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

        // add
        while (reader.hasNextLine()) {
            String line = reader.nextLine().trim();

            if (!line.isEmpty() && line.toCharArray()[0] != '#') {
                String[] argArray = line.split(" ");
                List<String> args = new ArrayList<>();
                Collections.addAll(args, argArray);

                this.addCommand(args);
            }
        }
    }

    @Override
    public void runCommand(MessageReceivedEvent event, List<String> args) {
        // make sure issuer is admin
        if (event.getAuthor().getPermissionsForGuild(event.getGuild()).contains(Permissions.ADMINISTRATOR)) {

            int status = this.addCommand(args);
            switch (status) {
                case (0):
                    Moby.reply(event, "Alias added successfully.");
                    break;

                case (1):
                    Moby.reply(event, "Not enough arguments, at the very " +
                            "least there must be the name of the alias you want to make and a command " +
                            "to reference it to.");
                    break;

                case (2):
                    Moby.reply(event, "This alias would overwrite an existing command.");
                    break;

                default:
                    Moby.reply(event, "An unknown error occurred.");
                    break;
            }
        } else {
            Moby.reply(event, CommandListener.REPLY_NOT_ADMIN);
        }
    }

    private int addCommand(List<String> args) {
        // check if not already used
        if (args.size() >= 1) {
            if (CommandListener.commandMap.get(args.get(0)) == null || CommandListener.commandMap.get(args.get(0)).isAliasedCommand()) {
                // valid call
                // extract names
                String aliasName = args.get(0);
                String commandName = args.get(1);
                Command command = CommandListener.commandMap.get(commandName);

                // extract args
                List<String> safeArgs = new ArrayList<>(args);
                safeArgs.remove(0); // remove the name
                safeArgs.remove(0); // remove the command

                // make command
                AliasedCommand aCmd = new AliasedCommand(aliasName, command, safeArgs);
                CommandListener.registerCommand(aCmd);

                return 0;

            } else if (args.size() < 1) {
                return 1;
            } else if (CommandListener.commandMap.get(args.get(0)) != null) {
                return 2;
            } else {
                return -1;
            }
        }
        return 2;
    }
}
