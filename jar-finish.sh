#
# A script to pack all of the relevant files that moby depends on
# into a archive, `moby.zip`. Note that this requires `token.txt`
# to be present in the root directory of the project. All other 
# required files should already be present from the repository.
#

# `out/artifacts/...` should be the directory where your .jar is outputted to, and
# `moby` is just some new directory where stuff will be further copied to
mkdir moby
cp out/artifacts/moby_jar/moby.jar moby/moby-T-1000.jar

# copy the necessary files to the folder
cp aliases.txt token.txt start.sh start.bat moby/

# compress everything for distribution
zip moby.zip moby/*

# clean up
rm -rf moby/
