# Moby T-1000

A Discord bot by Patrick Thomas.

## Usage (Linux)

### 0. Preface

I use JetBrain's IntelliJ under Linux with Java 8 to work on this project. Thus, all of my experiences are going to be related to such.

### 1. Downloading

Start by cloning this git repository or downloading the source:

```sh
git clone https://gitlab.com/patthomasrick/moby-bot-t-1000 
```

or

```sh
wget https://gitlab.com/patthomasrick/moby-bot-t-1000/-/archive/master/moby-bot-t-1000-master.zip 
unzip moby-bot-t-1000-master.zip
```

### 2. Running

This is a Maven project, so configuring this project in your favorite IDE that supports Maven is recommended. Otherwise, the main class is just `Moby`. In order to run, the project needs a text file, `token.txt`, in the root of the project. This is the Discord bot token, and [can be retrieved from this page.](https://discordapp.com/developers/applications/me). The format is just the token on the first line of `token.txt`, like so:

```
6qrZcUqja7812RVdnEKjpzOL4CvHBFG

```

